# Backend

## Getting Started

Install node_modules by `yarn`

## Create local database

Run below cmd step by step

### Install postgresql with brew

`brew install postgresql`

### Start postgresql service

`brew services start postgresql`

### Create api database

`CREATE DATABASE api;`
**api** is DB_NAME in .env

### Go to api DB and create duty table

`\c api`
`CREATE TABLE duty (
  ID SERIAL PRIMARY KEY,
  name VARCHAR(30)
);`

### Create admin role with password

Run `CREATE ROLE admin WITH LOGIN PASSWORD 'password';`
**admin** is DB_USER in .env
**password** is DB_PASSWORD in .env

### Grant superuser right to admin

`ALTER ROLE admin SUPERUSER`

### Connect api database with admin

`psql -d api -U admin`

## Start server in dev mode

Start server by `yarn dev`
Will run in port `8080`

## Run test

Run test by `yarn test`
