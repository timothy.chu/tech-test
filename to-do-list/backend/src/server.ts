import express, { Request, Response } from "express";
import { Pool } from "pg";
import cors from "cors";
import { config } from "dotenv";

config();

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: Number(process.env.DB_PORT),
});

const app = express();
const port = 8080;

const origin = {
  origin: "*",
};

app.use(express.json());
app.use(cors(origin));

// Create
app.post("/duties", async (req: Request, res: Response) => {
  const { name } = req.body;
  try {
    if (!name) {
      throw new Error("Missing variable");
    }

    const client = await pool.connect();
    const result = await client.query(
      "INSERT INTO duty (name) VALUES ($1) RETURNING *",
      [name]
    );
    const duty = result.rows[0];
    res.json(duty);
    client.release();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

// Read
app.get("/duties", async (req: Request, res: Response) => {
  try {
    const client = await pool.connect();
    const result = await client.query("SELECT * FROM duty ORDER BY id ASC");
    const duties = result.rows;
    res.json(duties);
    client.release();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

// Update
app.put("/duties/:id", async (req, res) => {
  const { id } = req.params;
  const { name } = req.body;

  try {
    if (!name || !id) {
      throw new Error("Missing variable");
    }

    const client = await pool.connect();
    const checkResult = await client.query("SELECT * FROM duty WHERE id = $1", [
      id,
    ]);

    if (checkResult.rows.length === 0) {
      res.status(404).json({ message: "Duty not found" });
    } else {
      const result = await client.query(
        "UPDATE duty SET name = $1 WHERE id = $2 RETURNING *",
        [name, id]
      );
      const updateDuty = result.rows[0];
      res.json(updateDuty);
    }

    client.release();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

// Delete
app.delete("/duties/:id", async (req, res) => {
  try {
    const { id } = req.params;

    const client = await pool.connect();
    const result = await client.query("DELETE FROM duty WHERE id = $1", [id]);

    if (result.rowCount === 0) {
      res.status(404).json({ message: "Duty not found" });
    } else {
      res.json({ message: "Duty deleted successfully" });
    }

    client.release();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

export default app;
