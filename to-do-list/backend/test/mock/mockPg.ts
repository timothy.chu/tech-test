interface mockPgOptions {
  queryResult?: {
    rows: {
      id: string;
      name: string;
    }[];
  };
}

export const mockPg = (options: mockPgOptions) => {
  const mockClient = {
    query: jest.fn().mockResolvedValue(options.queryResult),
    release: jest.fn(),
  };

  jest
    .spyOn(require("pg").Pool.prototype, "connect")
    .mockReturnValueOnce(mockClient as any);
};
