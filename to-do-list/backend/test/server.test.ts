import request from "supertest";
import app from "../src/server";
import { mockPg } from "./mock/mockPg";

describe("POST /duties", () => {
  it("should create a new duty", async () => {
    const dutyName = "New Duty";

    const queryResult = {
      rows: [
        {
          id: "1",
          name: dutyName,
        },
      ],
    };

    mockPg({
      queryResult,
    });

    const response = await request(app)
      .post("/duties")
      .send({ name: dutyName });

    expect(response.status).toBe(200);
    expect(response.body.name).toBe(dutyName);
    expect(response.body).toHaveProperty("id");
  });

  it("should return 500 if name is missing", async () => {
    const response = await request(app).post("/duties").send({});

    expect(response.status).toBe(500);
  });
});

describe("GET /duties", () => {
  it("should return all duties", async () => {
    const queryResult = {
      rows: [
        {
          id: "1",
          name: "name-1",
        },
        {
          id: "2",
          name: "name-2",
        },
      ],
    };

    mockPg({
      queryResult,
    });

    const response = await request(app).get("/duties");

    expect(response.status).toBe(200);
    expect(response.body.length).toBe(queryResult.rows.length);
    expect(Array.isArray(response.body)).toBe(true);
  });
});

describe("PUT /duties/:id", () => {
  it("should update a duty", async () => {
    const dutyId = "1";

    const queryResult = {
      rows: [
        {
          id: dutyId,
          name: "dutyName",
        },
      ],
    };

    mockPg({
      queryResult,
    });

    const updatedDutyName = "Updated Duty";

    const response = await request(app)
      .put(`/duties/${dutyId}`)
      .send({ name: updatedDutyName });

    expect(response.status).toBe(200);
    expect(response.body.id).toBe(dutyId);
  });

  it("should return 404 if duty is not found", async () => {
    const queryResult = {
      rows: [],
    };

    mockPg({
      queryResult,
    });

    const response = await request(app)
      .put("/duties/999")
      .send({ name: "Updated Duty" });

    expect(response.status).toBe(404);
    expect(response.body.message).toBe("Duty not found");
  });

  it("should return 500 if name or id is missing", async () => {
    const response = await request(app).put("/duties/1").send({});

    expect(response.status).toBe(500);
  });
});

describe("DELETE /duties/:id", () => {
  it("should delete a duty", async () => {
    const dutyId = "1";

    const queryResult = {
      rows: [
        {
          id: dutyId,
          name: "dutyName",
        },
      ],
    };

    mockPg({
      queryResult,
    });

    const response = await request(app).delete(`/duties/${dutyId}`);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Duty deleted successfully");
  });

  it("should return 404 if duty is not found", async () => {
    const response = await request(app).delete("/duties/999");

    expect(response.status).toBe(404);
    expect(response.body.message).toBe("Duty not found");
  });
});
