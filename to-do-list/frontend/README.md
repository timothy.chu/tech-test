# Frontend

## Getting Started

Install node_modules by `yarn`

## Run project

Run project by `yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Run test

Run test by `yarn test`

## Screenshot

![Screenshot](./public/images/screenshot.png)
