export interface Duty {
  id: string;
  name: string;
}

export interface DutyFormData {
  name: string;
}
