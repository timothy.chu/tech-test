import styled from "styled-components";
import { useDuty } from "../hooks/useDuty";
import ToDoItem from "./ToDoItem";

interface ToDoListProps {}

const ToDoList: React.FC<ToDoListProps> = (props) => {
  const { duties, updateDuty, createDuty, deleteDuty } = useDuty();

  const refreshDuties = () => {
    duties.mutate();
  };

  return (
    <ToDoListWrapper data-testid="to-do-list">
      <h1>{"To Do List"}</h1>
      <ToDoItem
        id={""}
        name={""}
        onCreateClicked={async (newDuty) => {
          await createDuty(newDuty);
          refreshDuties();
        }}
      />
      <hr></hr>
      {duties?.data?.map((duty) => {
        return (
          <ToDoItem
            key={`to-do-item-${duty.id}`}
            data-testid="to-do-item"
            id={duty.id}
            name={duty.name}
            onUpdateClicked={async (updatedDuty) => {
              await updateDuty(duty.id, updatedDuty);
              refreshDuties();
            }}
            onDeleteClicked={async () => {
              await deleteDuty(duty.id);
              refreshDuties();
            }}
          />
        );
      })}
    </ToDoListWrapper>
  );
};

const ToDoListWrapper = styled.div`
  flex-direction: column;
  gap: 4px;
  padding: 10px;
  display: flex;
`;

export default ToDoList;
