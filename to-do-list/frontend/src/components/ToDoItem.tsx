import { Button, Col, Row, Input } from "antd";
import styled from "styled-components";
import { useDutyForm } from "../hooks/useDutyForm";
import { useEffect } from "react";
import { DutyFormData } from "../types";

interface ToDoItemProps {
  id?: string;
  name: string;
  onCreateClicked?: (duty: DutyFormData) => void;
  onUpdateClicked?: (duty: DutyFormData) => void;
  onDeleteClicked?: () => void;
  "data-testid"?: string;
}

const ToDoItem: React.FC<ToDoItemProps> = (props) => {
  const {
    id,
    name,
    onCreateClicked,
    onUpdateClicked,
    onDeleteClicked,
    "data-testid": dataTestId,
  } = props;

  const { formMethods, resetForm } = useDutyForm({
    defaults: {
      name,
    },
  });

  const {
    setValue,
    handleSubmit,
    watch,
    formState: { errors },
  } = formMethods;

  const onSubmit = (formValue: DutyFormData) => {
    if (id) {
      // Update
      onUpdateClicked?.(formValue);
    } else {
      // Create
      onCreateClicked?.(formValue);
      setValue("name", "");
    }
  };

  useEffect(() => {
    if (id && name) {
      resetForm();
    }
  }, [id, name]);

  return (
    <ToDoItemWrapper gutter={[4, 4]} align="middle" data-testid={dataTestId}>
      <Col span={2}>{id}</Col>
      <Col span={10}>
        <Input
          value={watch("name")}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setValue("name", e.target.value);
          }}
          data-testid="name-input"
        />
        <div className="error-message">{errors.name?.message}</div>
      </Col>
      <Col span={6}>
        <Button
          className="button"
          onClick={handleSubmit(onSubmit)}
          type="primary"
          data-testid="submit-button"
        >
          {id ? "Save" : "Create"}
        </Button>
      </Col>
      {id && (
        <Col span={6}>
          <Button
            className="button"
            onClick={onDeleteClicked}
            danger
            type="primary"
            data-testid="delete-button"
          >
            {"Delete"}
          </Button>
        </Col>
      )}
    </ToDoItemWrapper>
  );
};

const ToDoItemWrapper = styled(Row)`
  & .button {
    width: 100%;
  }

  & .error-message {
    color: red;
  }
`;

export default ToDoItem;
