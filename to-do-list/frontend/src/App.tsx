import styled from "styled-components";
import ToDoList from "./components/ToDoList";

function App() {
  return (
    <AppContainer>
      <ToDoList />
    </AppContainer>
  );
}

export default App;

const AppContainer = styled.div`
  margin: 0 auto;
  max-width: 720px;
  width: 100vw;
  height: 100vh;
`;
