import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useCallback } from "react";
import { Duty, DutyFormData } from "../types";

export interface UseDutyFormOptions {
  defaults?: Partial<Duty>;
}

export const DutyFormValidationSchema = yup.object().shape({
  name: yup.string().required("Name is required"),
});

const defaultValues = {
  name: "",
};

export const useDutyForm = (options: UseDutyFormOptions = {}) => {
  const { defaults: restoreFormValue = {} as DutyFormData } = options;
  const formMethods = useForm<DutyFormData>({
    resolver: yupResolver(DutyFormValidationSchema),
    defaultValues,
  });

  const { reset } = formMethods;

  const resetForm = useCallback(() => {
    reset({
      ...defaultValues,
      ...restoreFormValue,
    });
  }, [restoreFormValue, reset, defaultValues]);

  return {
    formMethods,
    resetForm,
  };
};
