import { Duty, DutyFormData } from "../types";
import useSWR from "swr";
import axios from "axios";
import { useNotification } from "./useNotification";

const API_ENDPOINT = "http://localhost:8080";

const fetcher = async (url: string) => {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching the data.");
  }
};

export const useDuty = () => {
  const { showSuccessNotification, showErrorNotification } = useNotification();

  const { data, mutate } = useSWR<Duty[]>(`${API_ENDPOINT}/duties`, fetcher);

  const createDuty = (duty: DutyFormData) => {
    const { name } = duty;
    return axios
      .post(`${API_ENDPOINT}/duties`, { name })
      .then((response) => {
        showSuccessNotification("Duty created");
      })
      .catch((error) => {
        showErrorNotification(
          `Fail to create duty with error: ${error.message}`
        );
      });
  };

  const updateDuty = (id: string, duty: DutyFormData) => {
    const { name } = duty;
    return axios
      .put(`${API_ENDPOINT}/duties/${id}`, { name })
      .then((response) => {
        showSuccessNotification("Duty updated");
      })
      .catch((error) => {
        showErrorNotification(
          `Fail to update duty with error: ${error.message}`
        );
      });
  };

  const deleteDuty = (id: string) => {
    return axios
      .delete(`${API_ENDPOINT}/duties/${id}`)
      .then((response) => {
        showSuccessNotification("Duty deleted");
      })
      .catch((error) => {
        showErrorNotification(
          `Fail to delete duty with error: ${error.message}`
        );
      });
  };

  return {
    duties: {
      data,
      mutate,
    },
    createDuty,
    updateDuty,
    deleteDuty,
  };
};
