import { notification } from "antd";
import { CheckCircle, XCircle } from "react-bootstrap-icons";

export const useNotification = () => {
  const showSuccessNotification = (message: string) => {
    notification.success({
      message,
      icon: <CheckCircle color={"green"} size={28} />,
    });
  };

  const showErrorNotification = (message: string) => {
    notification.error({
      message,
      icon: <XCircle color={"red"} size={28} />,
    });
  };

  return {
    showSuccessNotification,
    showErrorNotification,
  };
};
