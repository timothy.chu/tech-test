import { Duty } from "../../../types";

export const createDuty = jest.fn(() => Promise.resolve({ ok: true }));
export const updateDuty = jest.fn(() => Promise.resolve({ ok: true }));
export const deleteDuty = jest.fn(() => Promise.resolve({ ok: true }));
export const mutate = jest.fn(() => Promise.resolve({ ok: true }));

interface mockUseDutyOptions {
  data?: Duty[];
}
export const mockUseDuty = (options: mockUseDutyOptions) => {
  const useDuty = jest.spyOn(require("@hooks/useDuty"), "useDuty");

  useDuty.mockImplementation(() => ({
    duties: {
      data: options?.data || [],
      mutate,
    },
    createDuty,
    updateDuty,
    deleteDuty,
  }));

  return useDuty;
};
