import { fireEvent, render, waitFor } from "@testing-library/react";
import ToDoItem from "../../components/ToDoItem";

export const onCreateClicked = jest.fn();
export const onUpdateClicked = jest.fn();
export const onDeleteClicked = jest.fn();

function setup(id: string) {
  const { getByTestId, getAllByTestId } = render(
    <ToDoItem
      id={id}
      name="name"
      onCreateClicked={onCreateClicked}
      onUpdateClicked={onUpdateClicked}
      onDeleteClicked={onDeleteClicked}
    />
  );

  return {
    getByTestId,
    getAllByTestId,
  };
}

describe("ToDoItem", () => {
  it("should call onCreateClicked when save button clicked when id is empty string", async () => {
    const { getByTestId } = setup("");
    const button = getByTestId("submit-button");
    const input = getByTestId("name-input");

    fireEvent.change(input, { target: { value: "name" } });
    fireEvent.click(button);

    await waitFor(() => {
      return expect(onCreateClicked).toHaveBeenCalled();
    });
  });

  it("should call onUpdateClicked when save button clicked when id is valid", async () => {
    const { getByTestId } = setup("001");
    const button = getByTestId("submit-button");
    const input = getByTestId("name-input");

    fireEvent.change(input, { target: { value: "new-name" } });
    fireEvent.click(button);

    await waitFor(() => {
      return expect(onUpdateClicked).toHaveBeenCalled();
    });
  });

  it("should call onDeleteClicked when delete button clicked when id is valid", async () => {
    const { getByTestId } = setup("001");
    const button = getByTestId("delete-button");

    fireEvent.click(button);

    await waitFor(() => {
      return expect(onDeleteClicked).toHaveBeenCalled();
    });
  });
});
