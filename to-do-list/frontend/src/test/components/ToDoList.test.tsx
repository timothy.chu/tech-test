import { render } from "@testing-library/react";
import ToDoList from "../../components/ToDoList";
import { Duty } from "../../types";
import { mockUseDuty } from "../mock/hooks/mockUseDuty";

function setup() {
  const { getByTestId, getAllByTestId } = render(<ToDoList />);

  return {
    getByTestId,
    getAllByTestId,
  };
}

const MOCK_DUTY_DATA: Duty[] = [
  {
    id: "1",
    name: "duty-1",
  },
  {
    id: "2",
    name: "duty-2",
  },
];

describe("ToDoList", () => {
  beforeEach(() => {
    mockUseDuty({
      data: MOCK_DUTY_DATA,
    });
  });

  it("should render correct number of to do item", () => {
    const { getAllByTestId } = setup();
    const numberOfToDoItem = getAllByTestId("to-do-item").length;
    expect(numberOfToDoItem).toEqual(MOCK_DUTY_DATA.length);
  });
});
